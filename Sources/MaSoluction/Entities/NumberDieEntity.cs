﻿namespace Entities
{
    public class NumberDieEntity
    {
        public long Id { get; set; }
        public int Name { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
    }
}