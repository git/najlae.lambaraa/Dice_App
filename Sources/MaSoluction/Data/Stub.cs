﻿using Model;


namespace Data
{
    public class Stub : IDataManager
    {
        private List<Die> listDice = new List<Die>()
        {
            new NumberDie("de1", 2, 6),
            new NumberDie("de2", 1, 6)
        };
        public bool AddDice(Die addD)
        {
            if (addD != null)
            {
                listDice.Add(addD);

                return true;
            }
            return false;
        }

        

        public void clear()
        {
            listDice.Clear();
        }

       

        public List<Die> GetDices()
        {
            
            return listDice;
        }

        public bool RemoveDice(Die removeD)
        {
            if (removeD!=null)
            {

                return listDice.Remove(removeD);
            }
            
            return false;
        }
    }
}