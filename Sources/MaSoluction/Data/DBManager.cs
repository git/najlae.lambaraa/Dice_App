﻿using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DBManager : IDataManager
    {
        public bool AddDice(Die addD)
        {
            bool resulta=false;
            using (var context = new NumberDieDbContext())
            {
                NumberDieEntity entity = new NumberDieEntity
                {
                    
                    

                };
                context.AddAsync(entity);
                resulta = context.SaveChanges() == 1;
            }
                return resulta;
        }

        public void clear()
        {
            using (var context = new NumberDieDbContext())
            {
                context.RemoveRange(context.NumberDice);
                context.SaveChanges();
            }
        }

        public List<Die> GetDices()
        {
            throw new NotImplementedException();
        }

        public bool RemoveDice(Die removeD)
        {
            bool reslta = false;
            using (var context = new NumberDieDbContext())
            {
                context.Remove(removeD);
                reslta= context.SaveChanges() == 1;
            }
            return reslta;

        }
    }


}
