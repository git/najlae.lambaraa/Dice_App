﻿using System.Diagnostics.CodeAnalysis;

namespace Model
{
    public abstract class Die
    {
        private String name ;
        /// <summary>
        /// Proprieté
        /// </summary>
        public string Name
        {
            get
            {
                return name;

            }
             protected set //pour que je puissse l'utilser sur la classe file
             {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Enter a name", nameof(value));
                }
                name = value;
             }
        }
        public Die(String name)
        {
            Name = name;
        }

        public abstract int RandomFace();
        
    }
}