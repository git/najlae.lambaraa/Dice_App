﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public interface IDataManager
    {
        public bool AddDice(Die addD);
        public bool RemoveDice(Die removeD);
        public void clear();
        
        public List<Die> GetDices();
    }
}
