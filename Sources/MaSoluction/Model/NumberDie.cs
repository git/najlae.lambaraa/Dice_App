﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class NumberDie : Die, IEquatable<NumberDie>
    {
        private int min;
        private int max;
        private static Random random=new Random();
        public NumberDie(string name,int min,int max) : base(name)
        {
            Name = name;
            Min = min;
            Max = max;
        }


        
        public int Min
        {
            get
            {
                return min;
            }
            private set
            {
                min = value;
            }
        }
        public int Max
        {
            get
            {
                return max;
            }
            private set
            {
                max = value;
            }
        }
        public override int RandomFace()
        {
            if (min < max)
            {
                int resultat = random.Next(min, max + 1);
                return resultat;
            }
            else throw new ArgumentException("le min doit etre inferieur de max ");
        }

       

        public bool Equals([AllowNull] NumberDie other)
        {
            return Name==other.Name && Min == other.Min && Max == other.Max; 
        }
        public override bool Equals(object? obj)
        {
            //Check si la reference passer est null
            if (ReferenceEquals(obj, null)) return false;
            //Check si reference passer is equality
            if (ReferenceEquals(obj, this)) return true;
            //Check if  types is equality
            if (GetType() != obj.GetType()) return false;
            return Equals(obj as NumberDie);
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Min.GetHashCode() ^ Max.GetHashCode();  
        }




    }
}
