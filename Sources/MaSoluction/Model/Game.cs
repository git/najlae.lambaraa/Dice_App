﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Game 
    {
        private string name;


        
        
        public ReadOnlyCollection<NumberDie> ListDice { get; set; }
        private List<NumberDie> listDice = new List<NumberDie>(); //encapsulation des collections voir les videos partie 2
        public Game(string name,IEnumerable<NumberDie> listDice)
        {
            Name = name;
            this.listDice.AddRange(listDice);

            ListDice=new ReadOnlyCollection<NumberDie>(this.listDice);
        }


        public Game(string name, params NumberDie[] listDice)
        {
            Name = name;
            this.listDice.AddRange(listDice);
            ListDice = new ReadOnlyCollection<NumberDie>(this.listDice);
        }
        /// <summary>
        /// Constructor Game
        /// </summary>
        public string Name
        {
            get
            {
                return name;

            }
            private set
            {
                //Indique si une chaîne spécifiée est null, vide ou se compose uniquement d'espaces blancs
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Enter a name", nameof(value));
                }
                name = value;
            }
        }


        public bool AddDice(NumberDie die)
        {
            if (listDice.Contains(die))
            {
                return false;
            }
            listDice.Add(die);
            return true; 
        }

        public IEnumerable<NumberDie> AddDices(params NumberDie[] die)
        {
            List<NumberDie> result = new();
            foreach(var i in die)
            {
                if (AddDice(i))
                {
                    result.Add(i);
                }
                
            }
            return result;
        }
        

    }
}
