﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model
{
    public class Manager
    {
        
        public IDataManager dataManager { get; set; }
        
        public Manager(IDataManager data)
        {
            dataManager = data;
        }

        public bool AddDice(Die addD)
        {
           if(dataManager!=null)
           {
                return dataManager.AddDice(addD);
                
           }
           return false;
          
            
        }
        public bool RemoveDice(Die removeD)
        {
            if (dataManager != null)
            {
                
                return dataManager.RemoveDice(removeD);
            }
            return false;
        }
        
        public List<Die> GetDice()
        {
            return dataManager.GetDices();
        }

    }
}
