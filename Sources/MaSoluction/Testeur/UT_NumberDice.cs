﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Testeur
{
    public class UT_NumberDice
    {
        [Theory]
        [InlineData(true, "De1", "De1" ,1,6,1,6)]
        [InlineData(false, "", "", 1, 7, 1, 6)]

        public void TestConstructor(bool isValid,string expectedName, string name ,int expectedMin,int expectedMax,int min,int max)
        {

            if (!isValid)
            {
                Assert.Throws<ArgumentException>(() => new NumberDie(name,min, max));
                return;
            }
            NumberDie d = new NumberDie(name,min,max);
            Assert.Equal(expectedMin, d.Min);
            Assert.Equal (expectedMax, d.Max);
            Assert.Equal( expectedName,d.Name);
        }

        public static IEnumerable<object[]> TestAddNumberDieToGame()
        {
            yield return new object[] {
                true,
                new NumberDie[]
                {
                    new NumberDie("de1",1,6),
                    new NumberDie("de2", 1, 6),
                    new NumberDie("de3", 1, 6),
                    new NumberDie("de4", 1, 6)
                },
                new Game("Monopoly", new NumberDie("de1",1,6),
                    new NumberDie("de2", 1, 6),
                    new NumberDie("de3", 1, 6)),
                new NumberDie("de4", 1, 6)

            };
            yield return new object[] {

                false,
                 new NumberDie[]
                 {
                    new NumberDie("de1",1,6),
                    new NumberDie("de2", 1, 6),
                    new NumberDie("de3", 1, 6),

                 },
                 new Game("Parchis",
                    new NumberDie("de1",1,6),
                    new NumberDie("de2", 1, 6),
                    new NumberDie("de3", 1, 6)),
                    new NumberDie("de3", 1, 6)
             };
           

        }
        [Theory]
        [MemberData(nameof(TestAddNumberDieToGame))]
        public void Test_AddGame(bool expectedResult, IEnumerable<NumberDie>expectedDie,Game game,NumberDie die)
        {
            bool resultat = game.AddDice(die);
            Assert.Equal(expectedResult, resultat);
            Assert.Equal(expectedDie.Count(),game.ListDice.Count());
            Assert.All(expectedDie, i => game.ListDice.Contains(i));
        }

    }
}
