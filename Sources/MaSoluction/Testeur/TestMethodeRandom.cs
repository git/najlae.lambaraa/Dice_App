﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Testeur
{
    public class TestMethodeRandom
    {
        [Fact]
        public void TesterMethodeRandom()
        {
            for (int i = 0; i > 100; i++)
            {
                //Arrange 
                NumberDie nd = new NumberDie("de1", 2, 13);
                //Act
                int actual = nd.RandomFace();
                int expectedMin = nd.Min;
                int expectedMax = nd.Max;

                //Assert

                Assert.InRange(actual, expectedMin, expectedMax);
            }


        }
        [Fact]
        public void TesterMethodeRandomNull()
        {
            //Arrange 
            NumberDie nd = new NumberDie("de1", 14, 13);

            Assert.Throws<ArgumentException>(() => nd.RandomFace());
        }
    }
}