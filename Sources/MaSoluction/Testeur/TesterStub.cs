﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Model;

namespace Testeur
{
    public class TesterStub
    {
        [Fact]
        public void TesterAddManager()
        {
            //Arange
            Manager m = new Manager(new Stub());
            NumberDie nd = new NumberDie("de1",1,6);
           // Acct
            //m.AddDice(nd);
            m.dataManager.AddDice(nd);
            //Assert
            Assert.Equal("de1", nd.Name);
           
        }
        [Fact]
        public void TesterRemoveManager()
        {
            //Arange
            Manager m = new Manager(new Stub());

            int n = m.GetDice().Count();
            NumberDie nd = new NumberDie("de1", 2, 6);
            // Acct
            // m.RemoveDice(nd);
            m.dataManager.RemoveDice(nd);
            //Assert
            Assert.NotEqual(n, m.GetDice().Count);
        }

        [Fact]
        public void TesterNullMethodeRandom()
        {
            //Arange
            Manager m = new Manager(new Stub());

            int n = m.GetDice().Count();
            NumberDie nd = null;

            Assert.False(m.AddDice(nd));
           
            Assert.False(m.RemoveDice(nd));

        }
    }
}
