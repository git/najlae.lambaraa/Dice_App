using Model;

namespace Testeur
{
    public class UT_ConstructeurGame
    {
        [Fact]
        public void TestConstructeurGame()
        {
            Game G = new Game("Monopoly");
            Assert.NotNull(G);
            Assert.Equal("Monopoly",G.Name);
        }
    }
}